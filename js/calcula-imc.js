var titulo = document.querySelector(".titulo");

titulo.textContent = "Aparecida nutricionista"
//traz todos os tr 
var pacientes = document.querySelectorAll(".paciente");

for(var i = 0; i < pacientes.length; i++){


    var paciente = pacientes[i];

    var tdPeso = paciente.querySelector(".info-peso");
    var peso = tdPeso.textContent;

    var tdAltura = paciente.querySelector(".info-altura");
    var altura = tdAltura.textContent;

    var tdImc = paciente.querySelector(".info-imc");

    var pesoEhValido = validaPeso(peso);
    var alturaEhValida = validaAltura(altura);

    if(!pesoEhValido){
        console.log("Peso inválido!");
        pesoEhValido = false;
        tdImc.textContent = "Peso inválido";
        paciente.classList.add("paciente-invalido");

    }
    if(!alturaEhValida){
        console.log("altura inválida!");
        alturaEhValido = false;
        tdImc.textContent = "altura inválida";
        paciente.classList.add("paciente-invalido");
    }
    if(alturaEhValida && pesoEhValido){
        var imc = calculaImc(peso, altura);
        //toFixed p/ manipular numero de casas decimais a serem exibidas
        tdImc.textContent = imc;
    }   

    function calculaImc(peso, altura){
        var imc = 0;
    
        imc = peso / (altura * altura);
    
        return imc.toFixed(2);
    }
    
}


function validaAltura(altura){
    if(altura >= 0 && altura<=4){
        return true;
    }else{
        return false;
    }
}
function validaPeso(peso){
    if(peso >= 0 && peso <= 500){
        return true;
    }else{
        return false;
    }
}
//evento de escutar tag
titulo.addEventListener("click", function(){
console.log("função anonima");
})

