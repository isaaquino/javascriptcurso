var botaoAdicionar = document.querySelector("#adicionar-paciente");
botaoAdicionar.addEventListener("click", function(event){
    //previne o comportamento padrão da page como atualizar um pagina ao clicar em um botão do form
    event.preventDefault();

    var form = document.querySelector("#form-adiciona");

    //extraindo informações do paciente do form
    var paciente = obtemPacienteDoFormulario(form);

    console.log(paciente);

    //cria a tr a td do paciente
    var pacienteTr = montaTr(paciente);

    console.log(pacienteTr);

    var erros = validaPaciente(paciente);
    

    if(erros.length > 0 ){
        exibeMensagensDeErros(erros);
        mensagemErro.textContent = erro;
        return;
    }

    var tabela = document.querySelector("#tabela-pacientes");

    tabela.appendChild(pacienteTr);
    var ul = document.querySelector("#mensagem-erro");

    ul.innerHTML = "";
    
    form.reset();

});

function obtemPacienteDoFormulario(form){

    var paciente = {
        nome: form.nome.value,
        peso: form.peso.value,
        altura: form.altura.value,
        gordura: form.gordura.value,
        imc: calculaImc(form.peso.value, form.altura.value)
    }

    return paciente;
}

function montaTr(paciente){
    var pacienteTr = document.createElement("tr");
    pacienteTr.classList.add("paciente");

    //coloca elementos filhos dentro da tabela pai
    pacienteTr.appendChild(montaTd(paciente.nome, "info-nome")); 
    pacienteTr.appendChild(montaTd(paciente.peso, "info-peso")); 
    pacienteTr.appendChild(montaTd(paciente.altura, "info-altura")); 
    pacienteTr.appendChild(montaTd(paciente.gordura, "info-gordura")); 
    pacienteTr.appendChild(montaTd(paciente.imc, "info-imc"));

    return pacienteTr;
}

function montaTd(dado, classe){
    var td = document.createElement("td");
    td.textContent = dado;
    td.classList.add(classe);

    return td;
}

function exibeMensagensDeErros(erros){
    var ul = document.querySelector("#mensagem-erro");
    //limpar a ul 
    ul.innerHTML = "";
    erros.forEach(function(erro){
        //cria a li
        var li = document.createElement("li");
        //insere no texto
        li.textContent = erro;
        //coloca conteudo da li na ul
        ul.appendChild(li);
    });
}

function validaPaciente(paciente){
    //array de erros
    var erros = [];

    if( paciente.nome.length == 0) erros.push("O nome não pode ser em branco");


    if(!validaPeso(paciente.peso))
         //joga para dentro do array de erros 
         erros.push("Peso é inválido");

    if(!validaAltura(paciente.altura)) erros.push("Altura é inválida!");
    
    if( paciente.gordura.length == 0) erros.push("O campo gordura não pode ser em branco");

    if( paciente.peso.length == 0) erros.push("O campo peso não pode ser em branco");

    if( paciente.altura.length == 0) erros.push("O campo altura não pode ser em branco");

    return erros;
}